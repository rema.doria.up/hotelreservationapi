const express = require('express');
const cors = require('cors');
const mongoose = require ('mongoose')
const bcrypt = require ('bcryptjs')
require('dotenv').config()
const port = 4000;
const User = require ('./models/User.js')
const Place = require ('./models/Place.js')
const app = express();
const jwt = require ('jsonwebtoken')
const cookieParser = require ('cookie-parser')
const LoginHistory = require('./models/LoginHistory.js');
const bcryptSalt = bcrypt.genSaltSync(10)
const jwtSecret = 'qwwqqqqqfassssssssss'
const moment = require('moment-timezone');
const imageDownloader = require ('image-downloader')
const multer = require('multer');
const fs = require ('fs')


app.use('/uploads', express.static(__dirname + '/uploads'))
app.use(express.json());
app.use(cookieParser())
app.use(cors({
  credentials: true,
  origin: 'http://localhost:5173'
}));


mongoose.connect(process.env.MONGO_URL);



app.post('/register', async (req, res) => {
    const { name, email, password } = req.body;
    const hashedPassword = bcrypt.hashSync(password, bcryptSalt);
  
    try {
      const userDoc = await User.create({
        name,
        email,
        password: hashedPassword
      });
  
      res.json(userDoc);
    } catch (error) {
      console.error('Error creating user:', error);
      res.status(500).json({ error: 'An error occurred while registering.' });
    }
  });
  app.get('/register', async (req, res) => {
    try {
      const users = await User.find().select('-password'); 
      res.json(users);
    } catch (error) {
      console.error('Error fetching users:', error);
      res.status(500).json({ error: 'An error occurred while fetching users.' });
    }
  });

  app.post('/login', async (req, res) => {
    const { email, password } = req.body;
    const userDoc = await User.findOne({ email });
  
    if (userDoc) {
      const passOk = bcrypt.compareSync(password, userDoc.password);
      if (passOk) {

        const phtTimestamp = moment.tz('Asia/Manila');
  
        const loginHistoryRecord = new LoginHistory({
          email: userDoc.email,
          timestamp: phtTimestamp, 
        });
  
        await loginHistoryRecord.save();
  
        jwt.sign(
          {
            email: userDoc.email,
            id: userDoc._id,
          },
          jwtSecret,
          {},
          (err, token) => {
            if (err) throw err;
            res.cookie('token', token).json(userDoc);
          }
        );
      } else {
        res.status(422).json('pass not ok');
      }
    } else {
      res.json('not found');
    }
  });

  app.get('/profile', (req, res) =>{
    const {token} = req.cookies;
    if (token) {
      jwt.verify(token, jwtSecret, {},  async (err, userData)=>{
        if (err) throw err
        const {name, email, _id} = await User.findById(userData.id)
        res.json({name, email, _id})
      })
    } else{
      res.json(null)
    }
  })

  app.post('/logout', (req, res)=>{
    res.cookie('token', '').json(true)
  })
  
  app.post('/upload-by-link', async (req, res) => {
    const { link } = req.body;
    const newName = 'photo' + Date.now() + '.jpg';
    await imageDownloader.image({
      url: link,
      dest: __dirname + '/uploads/' + newName, 
    });
    res.json(newName);
  });

  const photosMiddleware = multer({dest:'uploads/'})
  app.post('/upload', photosMiddleware.array('photos', 100),(req, res)=>{
    const uploadedFiles = []
     for (let i = 0; i < req.files.length; i++){
      const {path, originalname} = req.files[i]
      const parts = originalname.split('.')
      const ext = parts[parts.length - 1]
      const newPath = path + '.' + ext
      fs.renameSync(path, newPath)
      uploadedFiles.push(newPath.replace('uploads', ''))
     }
     res.json(uploadedFiles)

  })

  app.post('/places', (req, res) =>{
    const {token} = req.cookies;
    jwt.verify(token, jwtSecret, {},  async (err, userData)=>{
      if (err) throw err
      await Place.create({
        owner:userData.id,
      })  
    })
   
  })

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${4000}`);
});


