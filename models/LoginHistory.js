const mongoose = require('mongoose');
const moment = require('moment-timezone'); // Import moment-timezone

const loginHistorySchema = new mongoose.Schema({
  email: String,
  timestamp: {
    type: Date,
    default: moment.tz('Asia/Manila'), // Set the default timestamp to PHT
  },
});

const LoginHistory = mongoose.model('Login', loginHistorySchema); // Use 'LoginHistory' as the model name

module.exports = LoginHistory;